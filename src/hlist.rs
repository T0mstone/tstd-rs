use crate::pair::Pair;
use crate::tint::{Next, TInt, Zero};

/// A trait encompassing the HList API
pub trait HList: Pair<Left = Self::Head, Right = Self::Tail> {
	type Head;
	type Tail: HList<Length = <Self::Length as TInt>::SatPrev>;

	fn head(&self) -> &Self::Head {
		self.split_ref().0
	}
	fn head_mut(&mut self) -> &mut Self::Head {
		self.split_mut().0
	}
	fn tail(&self) -> &Self::Tail {
		self.split_ref().1
	}
	fn tail_mut(&mut self) -> &mut Self::Tail {
		self.split_mut().1
	}

	type Length: TInt;

	type Zip<R: HList<Length = Self::Length>>: HList;
	fn zip<R: HList<Length = Self::Length>>(self, rhs: R) -> Self::Zip<R>;
}

/// Take a shared or unique reference to each element of a `HList`.
pub trait HListAsRef<'a>: HList {
	type AsRef: HList;
	type AsMut: HList;

	fn as_ref(&'a self) -> Self::AsRef;
	fn as_mut(&'a mut self) -> Self::AsMut;
}

/// The empty list
#[doc(alias = "Nil")]
pub type Empty = ();
/// The list composed of one element and then another HList
#[doc(alias = "Cons")]
pub type Link<T, L> = (T, L);

/// Simple helper function to help with self-documenting code
#[inline]
pub const fn empty() -> Empty {}
/// Simple helper function to help with self-documenting code
#[inline]
pub const fn link<T, L>(head: T, tail: L) -> Link<T, L> {
	(head, tail)
}

#[allow(non_snake_case)]
#[macro_export]
macro_rules! HList {
    ($(,)?) => {
		()
	};
	($T:ty $(, $R:ty)* $(,)?) => {
		($T, $crate::HList![$($R),*])
	};
}

#[macro_export]
macro_rules! hlist {
    ($(,)?) => {
		()
	};
	($e:expr $(, $r:expr)* $(,)?) => {
		($e, $crate::hlist![$($r),*])
	};
}

#[macro_export]
macro_rules! hlist_pat {
    ($(,)?) => {
		()
	};
	($p:pat $(, $r:pat)* $(,)?) => {
		($p, $crate::hlist_pat![$($r),*])
	};
}

impl HList for Empty {
	type Head = ();
	type Tail = Empty;
	type Length = Zero;

	type Zip<R: HList<Length = Self::Length>> = Empty;
	fn zip<R: HList<Length = Self::Length>>(self, _rhs: R) -> Self::Zip<R> {
		empty()
	}
}
impl<T, L: HList> HList for Link<T, L> {
	type Head = T;
	type Tail = L;
	type Length = Next<L::Length>;

	type Zip<R: HList<Length = Self::Length>> =
		Link<(Self::Head, R::Head), <Self::Tail as HList>::Zip<R::Tail>>;
	fn zip<R: HList<Length = Self::Length>>(self, rhs: R) -> Self::Zip<R> {
		let (rhead, rtail) = rhs.split();
		link((self.0, rhead), self.1.zip(rtail))
	}
}

impl<'a> HListAsRef<'a> for Empty {
	type AsRef = Empty;
	type AsMut = Empty;

	fn as_ref(&'a self) -> Self::AsRef {}

	fn as_mut(&'a mut self) -> Self::AsMut {}
}
impl<'a, T: 'a, L: HListAsRef<'a>> HListAsRef<'a> for Link<T, L> {
	type AsRef = Link<&'a T, L::AsRef>;
	type AsMut = Link<&'a mut T, L::AsMut>;

	fn as_ref(&'a self) -> Self::AsRef {
		(&self.0, self.1.as_ref())
	}

	fn as_mut(&'a mut self) -> Self::AsMut {
		(&mut self.0, self.1.as_mut())
	}
}
