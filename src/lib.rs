#![cfg_attr(not(feature = "std"), no_std)]

/// A general `Pair` trait
pub mod pair;
/// A type-level heterogeneous linked list.
pub mod hlist;
/// A type-level integer
pub mod tint;

/// Generalizations and extensions of [`core::ops`]
pub mod ops;
/// Symbolic values and operations
pub mod sym_ops;