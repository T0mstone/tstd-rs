use crate::hlist::{HList, HListAsRef};

/// A general kind operation.
/// `O` denotes the kind of operation and `Self` is a [`HList`] of arguments.
pub trait Operation<O>: HList {
	type Output;

	fn op(self) -> Self::Output;
}

/// Shorthand trait for operations that act on references,
/// but have the same output regardless of lifetime.
pub trait RefOperation<O>: HList {
	type Output;

	fn op(&self) -> Self::Output;
}
impl<L: HList, Output, O> RefOperation<O> for L
where
	L: for<'a> HListAsRef<'a>,
	for<'a> <L as HListAsRef<'a>>::AsRef: Operation<O, Output = Output>,
{
	type Output = Output;

	fn op(&self) -> Self::Output {
		self.as_ref().op()
	}
}

#[cfg(not(feature = "num-traits"))]
macro_rules! invoke_primitives {
	(@gen[] $($r:tt)*) => {};
	(@gen[$t:ty $(, $r:ty)*] $macro:ident; $($args:tt)*) => {
		$macro!($t; $($args)*);
		invoke_primitives!(@gen[$($r),*] $macro; $($args)*);
	};
	($macro:ident!(/* empty */; $($args:tt)*);) => {};
    ($macro:ident!(u* $(, $ts:ident*)*; $($args:tt)*);) => {
		invoke_primitives!(@gen[u8, u16, u32, u64, u128, usize] $macro; $($args)*);
		invoke_primitives!($macro!($($ts*),*; $($args)*););
	};
	($macro:ident!(i* $(, $ts:ident*)*; $($args:tt)*);) => {
		invoke_primitives!(@gen[i8, i16, i32, i64, i128, isize] $macro; $($args)*);
		invoke_primitives!($macro!($($ts*),*; $($args)*););
	};
	($macro:ident!(f* $(, $ts:ident*)*; $($args:tt)*);) => {
		invoke_primitives!(@gen[f32, f64] $macro; $($args)*);
		invoke_primitives!($macro!($($ts*),*; $($args)*););
	};
}

pub mod unary_op {
	use core::marker::PhantomData;

	use crate::ops::Operation;
	use crate::{hlist_pat, HList};

	pub mod traits {
		pub trait Inv {
			type Output;

			fn inv(self) -> Self::Output;
		}

		pub trait Abs {
			type Output;

			fn abs(self) -> Self::Output;
		}

		pub trait Signum {
			type Output;

			fn signum(self) -> Self::Output;
		}

		#[cfg(feature = "num-traits")]
		const _IMPLS: () = {
			impl<T: num_traits::Inv> Inv for T {
				type Output = T::Output;

				#[inline]
				fn inv(self) -> Self::Output {
					num_traits::Inv::inv(self)
				}
			}

			impl<T: num_traits::Signed> Abs for T {
				type Output = T;

				#[inline]
				fn abs(self) -> Self::Output {
					num_traits::Signed::abs(&self)
				}
			}

			impl<T: num_traits::Signed> Signum for T {
				type Output = T;

				#[inline]
				fn signum(self) -> Self::Output {
					num_traits::Signed::signum(&self)
				}
			}
		};
		#[cfg(not(feature = "num-traits"))]
		const _IMPLS: () = {
			macro_rules! impl_float_inv {
				($f:ty;) => {
					impl Inv for $f {
						type Output = $f;

						#[inline]
						fn inv(self) -> Self::Output {
							1.0 / self
						}
					}
				};
			}
			invoke_primitives!(impl_float_inv!(f*;););

			macro_rules! impl_abs_signum {
				($t:ty; -) => {
					impl Abs for $t {
						type Output = $t;

						#[inline]
						fn abs(self) -> Self::Output {
							self
						}
					}

					impl Signum for $t {
						type Output = $t;

						#[inline]
						fn signum(self) -> Self::Output {
							if self == 0 {
								0
							} else {
								1
							}
						}
					}
				};
				($t:ty; $abs:ident, $signum:ident) => {
					impl Abs for $t {
						type Output = $t;

						#[inline]
						fn abs(self) -> Self::Output {
							<$t>::$abs(self)
						}
					}

					impl Signum for $t {
						type Output = $t;

						#[inline]
						fn signum(self) -> Self::Output {
							<$t>::$signum(self)
						}
					}
				};
			}

			invoke_primitives!(impl_abs_signum!(u*; -););
			invoke_primitives!(impl_abs_signum!(i*; abs, signum););
			#[cfg(feature = "std")]
			invoke_primitives!(impl_abs_signum!(f*; abs, signum););
		};
	}

	pub struct Clone(());
	pub struct Into<R>(PhantomData<R>);

	pub struct Neg(());
	pub struct Inv(());
	pub struct Abs(());
	pub struct Signum(());

	impl<'a, T: core::clone::Clone> Operation<Clone> for HList![&'a T] {
		type Output = T;

		fn op(self) -> Self::Output {
			let hlist_pat![value] = self;
			value.clone()
		}
	}
	impl<T: core::convert::Into<U>, U> Operation<Into<U>> for HList![T] {
		type Output = U;

		fn op(self) -> Self::Output {
			let hlist_pat![value] = self;
			value.into()
		}
	}

	macro_rules! impl_ {
	    ($($P:ident)::*: /* end */) => {};
		($($P:ident)::*: $Tr:ident::$meth:ident $(, $($rest:tt)*)?) => {
			impl<T: $($P)::*::$Tr> Operation<$Tr> for HList![T] {
				type Output = T::Output;

				#[inline]
				fn op(self) -> Self::Output {
					let hlist_pat![value] = self;
					$($P)::*::$Tr::$meth(value)
				}
			}
			impl_!($($P)::*: $($($rest)*)?);
		};
	}

	impl_!(core::ops: Neg::neg);
	impl_!(traits: Inv::inv, Abs::abs, Signum::signum);
}

pub mod binary_op {
	use crate::ops::Operation;
	use crate::{hlist_pat, HList};

	pub mod traits {
		pub trait Pow<Rhs> {
			type Output;

			fn pow(self, rhs: Rhs) -> Self::Output;
		}

		#[cfg(feature = "num-traits")]
		const _POW_IMPL: () = {
			impl<T: num_traits::Pow<P>, P> Pow<P> for T {
				type Output = T::Output;

				#[inline]
				fn pow(self, rhs: P) -> Self::Output {
					num_traits::Pow::pow(self, rhs)
				}
			}
		};
		#[cfg(not(feature = "num-traits"))]
		const _POW_IMPL: () = {
			macro_rules! impl_pow {
				($t:ty; $fname:ident, $rhs:ty) => {
					impl Pow<$rhs> for $t {
						type Output = $t;

						#[inline]
						fn pow(self, rhs: $rhs) -> Self::Output {
							<$t>::$fname(self, rhs)
						}
					}
				};
			}

			invoke_primitives!(impl_pow!(u*, i*; pow, u32););
			#[cfg(feature = "std")]
			const _FLOAT_POW: () = {
				invoke_primitives!(impl_pow!(f*; powi, i32););
				invoke_primitives!(impl_pow!(f*; powf, Self););
			};
		};
	}

	pub struct Add(());
	pub struct Sub(());
	pub struct Mul(());
	pub struct Div(());
	pub struct Pow(());

	macro_rules! impl_ {
		($($P:ident)::*: $(,)?) => {};
	    ($($P:ident)::*: $Tr:ident::$meth:ident $(, $($rest:tt)*)?) => {
			impl<T: $($P)::*::$Tr<U>, U> Operation<$Tr> for HList![T, U] {
				type Output = T::Output;

				#[inline]
				fn op(self) -> Self::Output {
					let hlist_pat![lhs, rhs] = self;
					$($P)::*::$Tr::$meth(lhs, rhs)
				}
			}
			impl_!($($P)::*: $($($rest)*)?);
		};
	}

	impl_!(
		core::ops:
			Add::add,
			Sub::sub,
			Mul::mul,
			Div::div
	);
	impl_!(traits: Pow::pow);
}
