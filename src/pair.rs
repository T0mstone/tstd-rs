/// Something that can roughly act like a pair.
pub trait Pair {
	type Left;
	type Right;

	fn new(left: Self::Left, right: Self::Right) -> Self;

	fn split(self) -> (Self::Left, Self::Right);
	fn split_ref(&self) -> (&Self::Left, &Self::Right);
	fn split_mut(&mut self) -> (&mut Self::Left, &mut Self::Right);
}

impl Pair for () {
	type Left = ();
	type Right = ();

	fn new((): Self::Left, (): Self::Right) {}

	fn split(self) -> (Self::Left, Self::Right) {
		((), ())
	}

	fn split_ref(&self) -> (&Self::Left, &Self::Right) {
		(&(), &())
	}

	fn split_mut(&mut self) -> (&mut Self::Left, &mut Self::Right) {
		static mut RHS: () = ();
		// SAFETY: A unit value can never change, and a mutable reference to it is also optimized away
		let rhs = unsafe { &mut RHS };
		(self, rhs)
	}
}

impl<L, R> Pair for (L, R) {
	type Left = L;
	type Right = R;

	fn new(left: Self::Left, right: Self::Right) -> Self {
		(left, right)
	}

	fn split(self) -> (Self::Left, Self::Right) {
		self
	}

	fn split_ref(&self) -> (&Self::Left, &Self::Right) {
		(&self.0, &self.1)
	}

	fn split_mut(&mut self) -> (&mut Self::Left, &mut Self::Right) {
		(&mut self.0, &mut self.1)
	}
}
