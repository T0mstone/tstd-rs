use core::fmt::{Debug, Formatter};
use core::marker::PhantomData;

use crate::hlist::{empty, link, Empty, HList, Link};
use crate::ops::binary_op::*;
use crate::{hlist, HList};

/// Evaluate a symbolic operation
pub trait Eval {
	type Output;

	fn eval(self) -> Self::Output;
}

impl Eval for () {
	type Output = ();

	fn eval(self) -> Self::Output {}
}

/// An already-evaluated value
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Ready<T>(pub T);

impl<T> Eval for Ready<T> {
	type Output = T;

	fn eval(self) -> Self::Output {
		self.0
	}
}

pub trait EvalHList: HList<Head = Self::EHead, Tail = Self::ETail> {
	type EHead: Eval;
	type ETail: EvalHList;

	type Output: HList;
	fn eval_all(self) -> Self::Output;
}
impl EvalHList for Empty {
	type EHead = ();
	type ETail = Empty;

	type Output = Empty;
	fn eval_all(self) -> Self::Output {
		empty()
	}
}
impl<T: Eval, L: EvalHList> EvalHList for Link<T, L> {
	type EHead = T;
	type ETail = L;

	type Output = Link<T::Output, L::Output>;
	fn eval_all(self) -> Self::Output {
		link(self.0.eval(), self.1.eval_all())
	}
}

/// An [`Operation`](crate::ops::Operation)
pub struct Operation<O, Args: HList>(pub PhantomData<O>, pub Args);

impl<O, Args: HList + Debug> Debug for Operation<O, Args> {
	fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
		struct DebugTypename<T>(PhantomData<T>);

		impl<T> Debug for DebugTypename<T> {
			fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
				write!(f, "{}", core::any::type_name::<T>())
			}
		}

		f.debug_tuple("Operation")
			.field(&DebugTypename::<O>(PhantomData))
			.field(&self.1)
			.finish()
	}
}

impl<O, Args: HList + Clone> Clone for Operation<O, Args> {
	fn clone(&self) -> Self {
		Self(PhantomData, self.1.clone())
	}
}

impl<O, Args: HList + Copy> Copy for Operation<O, Args> {}

pub type UnOp<O, T> = Operation<O, HList![T]>;
pub type BinOp<O, L, R> = Operation<O, HList![L, R]>;

impl<O, Args: HList + Default> Default for Operation<O, Args> {
	#[inline]
	fn default() -> Self {
		Self(PhantomData, Args::default())
	}
}

impl<O, Args: HList> Operation<O, Args> {
	#[inline]
	pub fn new(args: Args) -> Self {
		Self(PhantomData, args)
	}
}

impl<O, T> Operation<O, HList![T]> {
	#[inline]
	pub fn new1(t: T) -> Self {
		Self::new(hlist![t])
	}
}

impl<O, T, U> Operation<O, HList![T, U]> {
	#[inline]
	pub fn new2(t: T, u: U) -> Self {
		Self::new(hlist![t, u])
	}
}

impl<O, Args: EvalHList> Eval for Operation<O, Args>
where
	Args::Output: crate::ops::Operation<O>,
{
	type Output = <Args::Output as crate::ops::Operation<O>>::Output;

	fn eval(self) -> Self::Output {
		crate::ops::Operation::op(self.1.eval_all())
	}
}

macro_rules! impl_binops {
	([$($l:tt)*] * [$($r:tt)*] $([$($P:ident)::*]$Tr:ident::$meth:ident),*) => {
		impl_binops!(@tr {[$($l)*] * [$($r)*]} $([$($P)*]$Tr::$meth),*);
	};
	(@tr {$($args:tt)*} /* end */) => {};
    (@tr
		{$($args:tt)*}
		[$($P:ident)*]$Tr:ident::$meth:ident
		$(, $($rest:tt)*)?
	) => {
		impl_binops!(@prod[[$($P)*]$Tr::$meth] $($args)*);
		impl_binops!(@tr {$($args)*} $($($rest)*)?);
	};
	(@prod[$($pre:tt)*] [] * [$($r:tt)*]) => {
		// end
	};
	(@prod[$($pre:tt)*] [[$($lgen:tt)*] $l:ty $(where {$($wheres:tt)*})? $(, $($ls:tt)*)?] * [$($r:tt)*]) => {
		impl_binops!(@prodR[$($pre)*] [$($lgen)*] $l $(where {$($wheres)*})? [$($r)*]);
		impl_binops!(@prod[$($pre)*] [$($($ls)*)?] * [$($r)*]);
	};
	(@prodR[$($pre:tt)*] [$($lgen:tt)*] $l:ty $(where {$($wheres:tt)*})? []) => {
		// end
	};
	(@prodR[$($pre:tt)*] [$($lgen:tt)*] $l:ty $(where {$($lwheres:tt)*})? [[$($rgen:tt)*] $r:ty $(where {$($rwheres:tt)*})? $(, $($rs:tt)*)?]) => {
		impl_binops!(@one[$($pre)*] [$($lgen)*, $($rgen)*] {$($($lwheres)*,)? $($($rwheres)*)?} $l, $r);
		impl_binops!(@prodR[$($pre)*] [$($lgen)*] $l [$($($rs)*)?]);
	};
	(@one[[$($P:ident)*]$Tr:ident::$meth:ident] [$($gen:tt)*] {$($wheres:tt)*} $l:ty, $r:ty) => {
		impl<$($gen)*> $($P)::*::$Tr<$r> for $l
		where $($wheres)*
		{
			type Output = Operation<$Tr, HList![$l, $r]>;

			fn $meth(self, rhs: $r) -> Self::Output {
				Operation::new2(self, rhs)
			}
		}
	};
}

impl_binops!(
	[
		[T] Ready<T>,
		[O, Args: HList] Operation<O, Args>
	]
	* [
		[U] Ready<U>,
		[RO, RArgs: HList] Operation<RO, RArgs>
	]
	[core::ops]Add::add,
	[core::ops]Sub::sub,
	[core::ops]Mul::mul,
	[core::ops]Div::div,
	[crate::ops::binary_op::traits]Pow::pow
);
