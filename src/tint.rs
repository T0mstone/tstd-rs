use crate::hlist::{Empty, Link, link};

/// A type-level integer based on an `HList`
pub trait TInt: Default + Copy {
	/// The value of this integer as a `usize`, saturating to `usize::MAX` on overflow
	const AS_USIZE: usize;

	/// The previous integer to this one (saturating at 0)
	#[doc(alias = "Pred")]
	type SatPrev: TInt;

	fn sat_prev(self) -> Self::SatPrev;
}

pub type Zero = Empty;
pub type Next<N> = Link<(), N>;

/// Simple helper function to help with self-documenting code
#[inline]
pub fn zero() -> Zero {}
/// Simple helper function to help with self-documenting code
#[inline]
pub fn next<N>(n: N) -> Next<N> {
	link((), n)
}

pub type One = Next<Zero>;
pub type Two = Next<One>;

impl TInt for Zero {
	const AS_USIZE: usize = 0;
	type SatPrev = Zero;

	#[inline]
	fn sat_prev(self) -> Self::SatPrev {
		zero()
	}
}
impl<N: TInt> TInt for Next<N> {
	const AS_USIZE: usize = N::AS_USIZE.saturating_add(1);
	type SatPrev = N;

	#[inline]
	fn sat_prev(self) -> Self::SatPrev {
		self.1
	}
}
